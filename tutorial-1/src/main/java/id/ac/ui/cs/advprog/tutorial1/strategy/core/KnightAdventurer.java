package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
        //ToDo: Complete me

        public KnightAdventurer(){
            this.setAttackBehavior(new AttackWithSword());
            this.setDefenseBehavior(new DefendWithArmor());

            AttackBehavior attackBehavior = new AttackWithSword();
            DefenseBehavior defenseBehavior = new DefendWithArmor();
        }

        public String getAlias(){
            return ("Knight Adventurer");
        }
}
