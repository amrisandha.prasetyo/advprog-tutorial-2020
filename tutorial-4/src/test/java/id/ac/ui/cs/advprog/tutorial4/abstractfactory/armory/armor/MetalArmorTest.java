package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory.armor;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.MetalArmor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MetalArmorTest {

    Armor metalArmor;

    @BeforeEach
    public void setUp(){
        metalArmor = new MetalArmor();
    }

    @Test
    public void testToString(){
        // TODO create test
        assertTrue(metalArmor.getName().equals("Metal Armor"));
    }

    @Test
    public void testDescription(){
        // TODO create test
        assertTrue(metalArmor.getDescription().equals("This is a Metal Armor"));
    }
}
