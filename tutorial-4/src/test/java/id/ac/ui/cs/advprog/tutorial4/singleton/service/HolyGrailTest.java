package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {
    public HolyGrail holyGrail;

    // TODO create tests
    @BeforeEach
    public void setUp() throws Exception {
        holyGrail = new HolyGrail();
    }

    @Test
    public void testWish(){
        // TODO create test
        holyGrail.makeAWish("I am a wish");
        assertTrue(holyGrail.getHolyWish().getWish().equals("I am a wish"));
    }

    @Test
    public void testWishExist(){
        // TODO create test
        assertNotNull(holyGrail.getHolyWish());
    }
}
