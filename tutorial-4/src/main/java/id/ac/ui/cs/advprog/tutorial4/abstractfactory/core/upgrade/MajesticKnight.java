package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.MetalArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.ShiningArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ShiningForce;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ThousandYearsOfPain;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ShiningBuster;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ThousandJacker;

public class MajesticKnight extends Knight {

    public MajesticKnight(Armory armory) {
        this.armory = armory;
    }

    @Override
    public void prepare() {
        // TODO complete me
        this.armor = armory.craftArmor();
        this.weapon = armory.craftWeapon();
        this.skill = armory.learnSkill();
    }
}
