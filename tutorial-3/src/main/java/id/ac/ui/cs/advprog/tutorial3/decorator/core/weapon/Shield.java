package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Shield extends Weapon {
        //TODO: Complete me
        protected String weaponName = "Shield";
        protected String weaponDescription = "Heater Shield";
        public int weaponValue = 10;

        public String getName(){
            return weaponName;
        }

        public int getWeaponValue(){
            return weaponValue;
        }

        public String getDescription(){
            return weaponDescription;
        }
}
