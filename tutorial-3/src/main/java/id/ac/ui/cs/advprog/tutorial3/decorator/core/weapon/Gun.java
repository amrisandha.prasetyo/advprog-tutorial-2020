package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Gun extends Weapon {
        //TODO: Complete me

        protected String weaponName = "Gun";
        protected String weaponDescription = "Automatic Gun";
        public int weaponValue = 20;

        public String getName(){
            return weaponName;
        }

        public int getWeaponValue(){
            return weaponValue;
        }

        public String getDescription(){
            return weaponDescription;
        }

}
