package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;
    int upgradeValue;

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;
        Random rand = new Random();
        upgradeValue = 15;

    }

    @Override
    public String getName() {
        return "Magic " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        return (weapon != null ? weapon.getWeaponValue() : 0) + upgradeValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return ("Magic Upgrade");
    }
}
