package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
        //TODO: Complete me
        protected String weaponName = "Sword";
        protected String weaponDescription = "Great Sword";
        public int weaponValue = 25;

        public String getName(){
            return weaponName;
        }

        public int getWeaponValue(){
            return weaponValue;
        }

        public String getDescription(){
            return weaponDescription;
        }
}
