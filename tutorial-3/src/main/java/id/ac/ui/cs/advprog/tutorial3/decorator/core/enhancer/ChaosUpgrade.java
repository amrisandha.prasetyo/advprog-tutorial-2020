package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    int upgradeValue;


    public ChaosUpgrade(Weapon weapon) {
        this.weapon= weapon;
        upgradeValue = 50;
    }

    @Override
    public String getName() {
        return "Chaos " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        return (weapon != null ? weapon.getWeaponValue() : 0) + upgradeValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return ("Chaos Upgrade");
    }
}
