package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;
    int upgradeValue;

    public RawUpgrade(Weapon weapon) {

        this.weapon= weapon;
        Random rand = new Random();
        upgradeValue = 5;
    }

    @Override
    public String getName() {
        return "Raw " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        return (weapon != null ? weapon.getWeaponValue() : 0) + upgradeValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return ("Raw Upgrade");
    }
}
