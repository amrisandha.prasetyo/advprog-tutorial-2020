package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
        //TODO: Complete me
        protected String weaponName = "Longbow";
        protected String weaponDescription = "Big Longbow";
        public int weaponValue = 15;

        public String getName(){
            return weaponName;
        }

        public int getWeaponValue(){
            return weaponValue;
        }

        public String getDescription(){
            return weaponDescription;
        }
}
