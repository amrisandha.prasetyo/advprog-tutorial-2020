package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;



import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;
    int upgradeValue;

    public UniqueUpgrade(Weapon weapon) {
        this.weapon = weapon;
        Random rand = new Random();
        upgradeValue = 10;
    }

    @Override
    public String getName() {
        return "Unique "+  weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        return (weapon != null ? weapon.getWeaponValue() : 0) + upgradeValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return("Unique Upgrade");
    }
}
