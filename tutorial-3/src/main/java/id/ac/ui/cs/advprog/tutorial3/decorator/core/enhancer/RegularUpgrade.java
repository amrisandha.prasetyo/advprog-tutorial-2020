package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;
    int upgradeValue;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
        Random rand = new Random();
        upgradeValue = 1;
    }

    @Override
    public String getName() {
        return "Regular " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        return (weapon != null ? weapon.getWeaponValue() : 0) + upgradeValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return ("Regular Upgrade");
    }
}
