package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MagicUpgradeTest {


    private MagicUpgrade magicUpgrade;

    @BeforeEach
    public void setUp(){
        magicUpgrade = new MagicUpgrade(new Longbow());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals(this.magicUpgrade.getName(), "Magic Longbow");
    }

    @Test
    public void testGetMethodWeaponDescription(){
        //TODO: Complete me
        assertEquals(this.magicUpgrade.getDescription(), "Magic Upgrade");
    }


    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        assertTrue(this.magicUpgrade.getWeaponValue() >= 30 && this.magicUpgrade.getWeaponValue() <= 35);
    }
}
