package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Sword;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegularUpgradeTest {

    private RegularUpgrade regularUpgrade;

    @BeforeEach
    public void setUp(){
        regularUpgrade = new RegularUpgrade(new Sword());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals(this.regularUpgrade.getName(), "Regular Sword");
    }

    @Test
    public void testGetMethodWeaponDescription(){
        //TODO: Complete me
        assertEquals(this.regularUpgrade.getDescription(), "Regular Upgrade");
    }


    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        assertTrue(this.regularUpgrade.getWeaponValue() >= 26 && this.regularUpgrade.getWeaponValue() <= 30);
    }
}
