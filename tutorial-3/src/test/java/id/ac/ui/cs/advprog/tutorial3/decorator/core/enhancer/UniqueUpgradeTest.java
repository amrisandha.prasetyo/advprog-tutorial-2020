package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UniqueUpgradeTest {

    private UniqueUpgrade uniqueUpgrade;

    @BeforeEach
    public void setUp(){
        uniqueUpgrade = new UniqueUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals(this.uniqueUpgrade.getName(), "Unique Shield");
    }

    @Test
    public void testGetMethodWeaponDescription(){
        //TODO: Complete me
        assertEquals(this.uniqueUpgrade.getDescription(), "Unique Upgrade");
    }


    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        assertTrue(this.uniqueUpgrade.getWeaponValue() >= 20 && this.uniqueUpgrade.getWeaponValue() <= 25);
    }
}
