package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Asuna", "Waifu");
    }

    @Test
    public void testMethodGetName() {
        //TODO: Complete me
            assertEquals(member.getName(), "Asuna");
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals(member.getRole(), "Waifu");
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        //TODO: Complete me
        Member rock = new OrdinaryMember("Rock", "Lee");
        member.addChildMember(rock);
        int sizeAfterAddMember = member.getChildMembers().size();

        member.removeChildMember(rock);
        int sizeAfterRemoveMember = member.getChildMembers().size();

        assertEquals(sizeAfterAddMember, sizeAfterRemoveMember);
    }
}
