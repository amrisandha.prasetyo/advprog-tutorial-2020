package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Aqua", "Goddess");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals(member.getName(), "Aqua");
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals(member.getRole(), "Goddess");
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me

        Member sakura = new OrdinaryMember("Sakura", "Haruno");
        member.addChildMember(sakura);
        assertEquals(1, member.getChildMembers().size());

    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me

        Member minato = new OrdinaryMember("Minato", "Namikaze");
        member.addChildMember(minato);
        member.removeChildMember(minato);


        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member temari = new PremiumMember("Temari", "Sasori");
        temari.addChildMember(new OrdinaryMember("Tenten", "Haku"));
        temari.addChildMember(new OrdinaryMember("Konan", "Mitsuki"));
        temari.addChildMember(new OrdinaryMember("Kiba", "Inuzuka"));
        temari.addChildMember(new OrdinaryMember("Hidan", "Kakuzu"));

        assertEquals(temari.getChildMembers().size(), 3);
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member shikamaru = new PremiumMember("Shikamaru", "Master");
        shikamaru.addChildMember(new OrdinaryMember("Kurama", "Deidara"));
        shikamaru.addChildMember(new OrdinaryMember("Sarada", "Uchiha"));
        shikamaru.addChildMember(new OrdinaryMember("Ino", "Yamanaka"));
        shikamaru.addChildMember(new OrdinaryMember("Neji", "Hyuga"));

        assertEquals(shikamaru.getChildMembers().size(), 4);
    }
}
