package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Naruto", "Master");
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        Member sasuke = new OrdinaryMember("Sasuke", "Waifu");
        int guildSize = guild.getMemberList().size();
        guild.addMember(guildMaster, sasuke);
        assertEquals(guild.getMemberList().size(), guildSize+1);

    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member itachi = new OrdinaryMember("Itachi", "Wibu");
        guild.addMember(guildMaster, itachi);
        guild.removeMember(guildMaster, itachi);

        assertEquals(guild.getMemberList().size(), 1);
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Sasuke", "Uchiha");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member hinata = new OrdinaryMember("Hinata", "Senpai");
        guild.addMember(guildMaster, hinata);

        assertEquals(guild.getMember("Hinata", "Senpai").getName(), "Hinata");

    }
}
