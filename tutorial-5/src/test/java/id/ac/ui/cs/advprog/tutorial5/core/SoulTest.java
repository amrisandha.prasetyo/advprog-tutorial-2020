package id.ac.ui.cs.advprog.tutorial5.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class SoulTest {
    private Soul soul;

    @BeforeEach
    public void setUp(){
        Soul soul = new Soul("Name", 18, "F", "Occupation");
        soul.setId(1);
    }

    @Test
    void getGender() {
        assertEquals("F",soul.getGender());
    }

    @Test
    void setGender() {
        soul.setGender("M");
        assertEquals("M",soul.getGender());
    }

    @Test
    void getOccupation() {
        assertEquals("Occupation",soul.getOccupation());
    }

    @Test
    void setOccupation() {
        soul.setOccupation("Occupations");
        assertEquals("Occupations",soul.getOccupation());
    }

    @Test
    void getName() {
        assertEquals("Name",soul.getName());
    }

    @Test
    void setName() {
        soul.setName("Nama");
        assertEquals("Nama",soul.getName());
    }

    @Test
    void getAge() {
        assertEquals(18,soul.getAge());
    }

    @Test
    void setAge() {
        soul.setAge(19);
        assertEquals(19,soul.getAge());
    }

    @Test
    void getId() {
        assertEquals(1,soul.getId());
    }

    @Test
    void setId() {
        soul.setId(2);
        assertEquals(2,soul.getId());
    }

}
