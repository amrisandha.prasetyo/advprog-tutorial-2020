package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritStealthSpell extends HighSpiritSpell {
    // TODO: Complete Me

    public HighSpiritStealthSpell(HighSpirit spirit){
        super(spirit);
    }
    @Override
    public String spellName() {
        return this.spirit.getRace() + ":Stealth";
    }

    public void cast() {
        this.spirit.stealthStance();
    }
}
